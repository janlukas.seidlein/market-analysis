
package jpp.marketanalysis.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class CSVShareImporter implements ShareImporter {

	@Override
	public ShareCollection load(InputStream is) throws IOException {

		// TODO Auto-generated method stub

		ArrayList<Share> shareList = new ArrayList<Share>();
		Scanner sc1 = new Scanner(is);
		ArrayList<String> stringList = new ArrayList<String>();
		ArrayList<Integer> integer = new ArrayList<Integer>();
		while (sc1.hasNext()) {
			String l;
			if (!(l = sc1.nextLine()).isEmpty()) {
                //System.out.println(l);
				stringList.add(l);
				stringList.add("\n");
			}
		}
		stringList.add("done");
		for (int a = 3; a < stringList.size(); a++) {
			if (stringList.get(a).contains("SHARE")) {
				integer.add(a);
			}
		}
		for (Integer inte : integer) {
			stringList.add(inte, "\n");
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		String nl = "\n";

		for (String line : stringList) {

			baos.write(line.getBytes());
			// baos.write(nl.getBytes());
            
		}

		byte[] bytes = baos.toByteArray();
		InputStream in = new ByteArrayInputStream(bytes);
		Scanner sc = new Scanner(in);
		
		
		
		
		
		while (sc.hasNext()) {

			if (!sc.nextLine().equals("date;open;high;low;close")) { // 1.Line
				sc.close();
				throw new IOException("22");
			}

			String l = "";
            
			while (sc.hasNext() && (l = sc.nextLine()).contains("SHARE;")) {// 2.Line
				String name = (l.substring(6, l.length() - 3));
				if(l.length()-name.length()<4){
	             	throw new IOException("31");
	             }
				shareList.add(readShare(sc, name,l));
				
			}
            if(!l.contains("SHARE")&&l.contains(";;")){
            	throw new IOException("37");
            }
			ShareCollection sco = new ShareCollection(shareList);
			 
            //System.out.println(l);
            
			return sco;
		}
		sc.close();
		throw new IOException("46");
	}

	public Dataset readDataset(String line) throws IOException {
		//System.out.println(line);
		String[] split = line.split(";");
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		String date = split[0];

		LocalDate dateDate;
		if (date.length() == 10) {
			dateDate = LocalDate.parse(date, format);
		} else
			throw new IOException("47");

		double open = 0;
		double high = 0;
		double low = 0;
		double close = 0;

		try {
			open = Double.parseDouble(split[1]);
			high = Double.parseDouble(split[2]);
			low = Double.parseDouble(split[3]);
			close = Double.parseDouble(split[4]);

		} catch (NumberFormatException e) {
			throw new IOException();
		}
		Dataset d = new Dataset(dateDate, open, high, low, close);
		return d;
	}

	public Share readShare(Scanner sc, String name,String lo) throws IOException {
		// name
		//System.out.println(lo);
		if(lo.contains(";;;;")){
			throw new IOException();
		}
		boolean next = false;
		String shareName = name;
		ArrayList<Dataset> datasetList = new ArrayList<Dataset>();
		String l = "";
		if (sc.hasNext()) {
			l = sc.nextLine(); // 3.Line
			//System.out.println("l: "+l);
			while(l.isEmpty()){
				l =sc.nextLine();
			}
			next = true;
		} else {
			next = false;
		}
        //System.out.println(next);
		if (next) {
			//System.out.println(l);
			while(sc.hasNext()&& !l.contains("SHARE")&&!l.isEmpty()&&!l.contains("done")){
		    //System.out.println("in while: "+l);
			datasetList.add(readDataset(l));
			l = sc.nextLine();
			}
		} else {
			throw new IOException();
		}
        if(datasetList.isEmpty()||shareName.isEmpty()){
        	throw new IOException();
        }
		Share s = new Share(shareName, datasetList);
		return s;
	}

}
