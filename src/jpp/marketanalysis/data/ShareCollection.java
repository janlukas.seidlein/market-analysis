package jpp.marketanalysis.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class ShareCollection implements Iterable<Share> {
	ArrayList<Share> shares = new ArrayList<Share>();

	public ShareCollection() {

	}

	public ShareCollection(Collection<Share> shares) {
		boolean doublE = false;
		ArrayList<Share> test = new ArrayList<Share>(shares);

		for (int i = 0; i < shares.size(); i++) {
			for (int k = 0; k < shares.size(); k++) {
				if (test.get(i).equals(test.get(k)) && i != k) {
					doublE = true;
				}
			}

		}
		if (doublE) {
			throw new IllegalArgumentException();
		} else
			this.shares = test;

	}

	public boolean add(Share share) {
		boolean doublE = false;

		for (Share a : shares) {
			if (a.equals(share)) {
				doublE = true;
			}

		}
		if (doublE == false) {
			shares.add(share);
			return true;
		} else
			return false;

	}

	public Share get(String share) {
		// boolean contains = false;
		for (Share a : shares) {
			if (a.getName().equals(share)) {
				return a;
			}
		}
		return null;
	}

	public boolean remove(String name) {
		for (Share a : shares) {
			if (a.getName().equals(name)) {
				shares.remove(a);
				return true;
			}
		}
		return false;
	}

	public boolean exists(String name) {
		for (Share a : shares) {
			if (a.getName().equals(name)) {

				return true;
			}
		}
		return false;
	}

	public Collection<Share> getAll() {
		ArrayList<Share> list = new ArrayList<Share>(shares);
		return list;
	}

	public int size() {
		return shares.size();
	}

	public String toString() {
		Collections.sort(shares, new Comparator<Share>() {
			public int compare(Share s1, Share s2) {
				return s1.getName().compareTo(s2.getName());
			}
		});
		String result = "date;open;high;low;close";
		for (Share a : shares) {
			result = result + "\n" + a.toString();
		}
		return result;
	}

	@Override
	public Iterator<Share> iterator() {
		// TODO Auto-generated method stub
		return shares.iterator();
	}

}
