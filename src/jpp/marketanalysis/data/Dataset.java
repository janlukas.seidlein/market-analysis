package jpp.marketanalysis.data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Dataset  implements Comparable<Dataset>{
	LocalDate date;
	double open;
	double high;
	double low;
	double close;
	
	public Dataset(LocalDate date, double open,double high, double low , double close){
		if(date!= null){
			this.date = date;
			this.open = open ;
			this.high = high;
			this.low = low;
			this.close =  close;
		}else throw new NullPointerException();
	}

	public LocalDate getDate() {
		LocalDate dateReturn;
		dateReturn = date;
		return dateReturn;
	}
	
	public double getOpen() {
		return open;
	}

	public double getHigh() {
		return high;
	}

	public double getLow() {
		return low;
	}
	
	public double getClose() {
		return close;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(close);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		temp = Double.doubleToLongBits(high);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(low);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(open);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dataset other = (Dataset) obj;
		if (Double.doubleToLongBits(close) != Double.doubleToLongBits(other.close))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (Double.doubleToLongBits(high) != Double.doubleToLongBits(other.high))
			return false;
		if (Double.doubleToLongBits(low) != Double.doubleToLongBits(other.low))
			return false;
		if (Double.doubleToLongBits(open) != Double.doubleToLongBits(other.open))
			return false;
		return true;
	}

	@Override
	public String toString() {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy"); //MMM d yyyy  hh:mm a");
	    String out = date.format(format);
	    String resultOpen = String.format("%.2f", open);
	    String resultHigh = String.format("%.2f", high);
	    String resultLow = String.format("%.2f", low);
	    String resultClose = String.format("%.2f", close);
		String result = out+";"+resultOpen+";"+resultHigh+";"+resultLow+ ";"+resultClose;
		return result;
	}

	@Override
	public int compareTo(Dataset other) {
		return date.compareTo(other.getDate());
	}
    
	

}
