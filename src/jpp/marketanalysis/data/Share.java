package jpp.marketanalysis.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Share implements Comparable<Share> {
	String name;
	List<Dataset> data;

	public Share(String name, Collection<Dataset> data) {
		boolean doubleDate = false;

		for (Dataset d : data) {
			for (Dataset k : data) {
				if (d.getDate().equals(k.getDate()) && d.equals(k)== false) {
					doubleDate = true;
				}
			}}
			if (name == null || data == null) {
				throw new NullPointerException();
			}

		
			else if (data.size() == 0 || doubleDate) {
			throw new IllegalArgumentException();
		} 
		else{
			this.name = name;
			ArrayList <Dataset> list = new ArrayList<Dataset>(data);
			Collections.sort(list, new Comparator<Dataset>(){
				public int compare(Dataset s1, Dataset s2 ){
					return s1.getDate().compareTo(s2.getDate());
				}
			});
		    this.data = list;
		}

	}
	public String getName(){
		return name ;
	}
	public List<Dataset>getData(){
		ArrayList <Dataset> list = new ArrayList<Dataset>(data); //error
		Collections.sort(list, new Comparator<Dataset>(){
			public int compare(Dataset s1, Dataset s2 ){
				return s1.getDate().compareTo(s2.getDate());
			}
		});
		return list;
	}
    public Dataset getFirst(){
    	return data.get(0);
    }
    public Dataset getLast(){
    	return data.get(data.size()-1);
    }
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Share other = (Share) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
    public String toString(){
    	String result1;
    	result1 = "SHARE;"+name+";;;\n";
    	for (Dataset k : data) {
    		result1 = result1 + k.toString()+ "\n";
    	}
    	return result1.substring(0, result1.length()-1);
    	
    }
	@Override
	public int compareTo(Share o) {
		// TODO Auto-generated method stub
		return name.compareTo(o.getName());
	}
    
}
