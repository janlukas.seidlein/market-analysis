package jpp.marketanalysis.data;

import java.io.IOException;
import java.io.InputStream;

public interface ShareImporter {
	ShareCollection load(InputStream is) throws IOException;
		
	
	
}
