/*
 * Copyright (c) 2008, 2015, Oracle and/or its affiliates.
 * All rights reserved. Use is subject to license terms.
 *
 * This file is available and licensed under the following license:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the distribution.
 *  - Neither the name of Oracle Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package jpp.marketanalysis.gui.candlestick;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

/**
 * A custom candlestick chart. This sample shows how to extend XYChart base class
 * to create your own two axis chart type.
 */
public class CandleStickChartApp extends Application {

    // DAY, OPEN, HIGH, LOW, CLOSE
    private static double[][] data = new double[][]{
            {1,  25, 32, 16, 20},
            {2,  26, 33, 22, 30},
            {3,  30, 40, 20, 38},
            {4,  24, 34, 22, 30},
            {5,  26, 40, 24, 36},
            {6,  28, 45, 25, 38},
            {7,  36, 44, 28, 30},
            {8,  30, 36, 16, 18},
            {9,  40, 52, 36, 50},
            {10, 30, 38, 28, 34},
            {11, 24, 30,  8, 12},
            {12, 28, 46, 25, 40},
            {13, 28, 36, 14, 18},
            {14, 38, 40, 26, 30},
            {15, 28, 40, 28, 33},
            {16, 25, 32,  6, 10},
            {17, 26, 42, 18, 30},
            {18, 20, 30, 10, 18},
            {19, 20, 30,  5, 10},
            {20, 26, 32, 10, 16},
            {21, 38, 44, 32, 40},
            {22, 26, 41, 12, 40},
            {23, 30, 34, 10, 18},
            {24, 12, 26, 12, 23},
            {25, 30, 45, 16, 40},
            {26, 25, 38, 20, 35},
            {27, 24, 30,  8, 12},
            {28, 23, 46, 15, 44},
            {29, 28, 30, 12, 18},
            {30, 28, 30, 12, 18},
            {31, 28, 30, 12, 18}
    };

    private CandleStickChart chart;
    private CategoryAxis xAxis;
    private NumberAxis yAxis;

    public Parent createContent() {
        xAxis = new CategoryAxis();
        yAxis = new NumberAxis();
        chart = new CandleStickChart(xAxis,yAxis);
        // setup chart
        xAxis.setLabel("Day");
        yAxis.setLabel("Price");
        // add starting data
        XYChart.Series<String,Number> series = new XYChart.Series<>();
        for (int i=0; i< data.length; i++) {
            double[] day = data[i];
            series.getData().add(
                new XYChart.Data<String,Number>(String.valueOf((int)day[0]),day[1],new CandleStickExtraValues(day[2],day[3],day[4]))
            );
        }
        ObservableList<XYChart.Series<String,Number>> data = chart.getData();
        if (data == null) {
            data = FXCollections.observableArrayList(series);
            chart.setData(data);
        } else {
            chart.getData().add(series);
        }
        return chart;
    }

    @Override public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(new Scene(createContent()));
        primaryStage.show();
    }

    /**
     * Java main for when running without JavaFX launcher
     * @param args command line arguments
     */
    public static void main(String[] args) { launch(args); }
}
