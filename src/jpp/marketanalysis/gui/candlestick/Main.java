package jpp.marketanalysis.gui.candlestick;

import jpp.marketanalysis.*;
import jpp.marketanalysis.analyser.ShareAnalyser;
import jpp.marketanalysis.data.CSVShareImporter;
import jpp.marketanalysis.data.Dataset;
import jpp.marketanalysis.data.Share;
import jpp.marketanalysis.data.ShareCollection;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.io.*;
import java.time.LocalDate;
import java.time.format.ResolverStyle;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class Main extends Application {
    private	GridPane g = new GridPane();
    private	TableView tabas ;
	private ShareCollection shareCollection = new ShareCollection();
	private Share now;
	private LocalDate dateFrom = LocalDate.MIN;
	private LocalDate dateTo = LocalDate.MAX;
	private boolean tabt1 = true;
	// DAY, OPEN, HIGH, LOW, CLOSE

	private CandleStickChart chart;
	private CategoryAxis xAxis;
	private NumberAxis yAxis;

	public Parent createContent(Share share) {
		if (share.getData().size() > 100) {
			ShareAnalyser ana = new ShareAnalyser();
			share = ana.getWeekly(share);

			if (share.getData().size() > 100) {
				share = ana.getMonthly(share);

			}
		}

		xAxis = new CategoryAxis();
		yAxis = new NumberAxis();
		yAxis.setAutoRanging(true);
		xAxis.setAutoRanging(true);
		chart = new CandleStickChart(xAxis, yAxis);
		// setup chart
		xAxis.setLabel("Day");
		yAxis.setLabel("Price");
		yAxis.setForceZeroInRange(false);
		// add starting data
		XYChart.Series<String, Number> series = new XYChart.Series<>();
		//System.out.println("test");
		for (int i = 0; i < share.getData().size(); i++) {

			series.getData().add(new XYChart.Data<String, Number>(String.valueOf(share.getData().get(i).getDate()),
					share.getData().get(i).getOpen(), new CandleStickExtraValues(share.getData().get(i).getLow(),
							share.getData().get(i).getHigh(), share.getData().get(i).getClose())));
		}
		ObservableList<XYChart.Series<String, Number>> data = chart.getData();
		if (data == null) {
			data = FXCollections.observableArrayList(series);
			chart.setData(data);
		} else {
			chart.getData().add(series);
		}
		TableView <Dataset> table = new TableView();
		
		TableColumn <Dataset,Double>close = new TableColumn<Dataset,Double>("Close: ");
		close.setMinWidth(200);
		close.setCellValueFactory(new PropertyValueFactory<>("close"));
		
		TableColumn <Dataset,Double>open = new TableColumn<Dataset,Double>("Open: ");
		open.setMinWidth(200);
		open.setCellValueFactory(new PropertyValueFactory<>("open"));
		
		TableColumn <Dataset,Double>high = new TableColumn<Dataset,Double>("High: ");
		high.setMinWidth(200);
		high.setCellValueFactory(new PropertyValueFactory<>("high"));
		
		TableColumn <Dataset,Double>low = new TableColumn<Dataset,Double>("Low: ");
		low.setMinWidth(200);
		low.setCellValueFactory(new PropertyValueFactory<>("low"));
		
		TableColumn <Dataset,LocalDate>date = new TableColumn<Dataset,LocalDate>("Date: "+share.getName());
		date.setMinWidth(200);
		date.setCellValueFactory(new PropertyValueFactory<>("date"));
		
		table = new TableView<>();
		ObservableList<Dataset> dataset = FXCollections.observableArrayList();
		for(int hey = 0; hey<share.getData().size();hey++){
			dataset.add(share.getData().get(hey));
		}
		table.setItems(dataset);
		table.getColumns().addAll(date,open,low,high,close);
	    
		tabas = table ;
		
		
		return chart;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		
		
        TabPane t = new TabPane();
        t.getSelectionModel().selectedItemProperty().addListener((ov, oldValue, newValue) ->{
        	
        	try {
				g.getChildren().remove(chart);
				g.add(tabas, 0, 1);
			} catch (NullPointerException n1) {
				// TODO Auto-generated catch block
			} catch (IllegalArgumentException i0){
				
			}
        });
        
        Tab tab1 = new Tab();
        tab1.setClosable(false);
        tab1.setText("Chart");
        
        Tab tab2 = new Tab();
        tab2.setClosable(false);
        tab2.setText("Table");
       
        t.getTabs().add(tab1);
        t.getTabs().add(tab2);
        g.add(t, 0, 0);
        tab1.setOnSelectionChanged(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
                if (tab1.isSelected()) {
                	try {
                		tabt1 = true;
        				g.getChildren().remove(tabas);
        				g.add(chart, 0, 2);
        			} catch (NullPointerException n1) {
        				// TODO Auto-generated catch block
        				
        			}catch(IllegalArgumentException i1){
        				
        			}
                }
            }
        });
        tab2.setOnSelectionChanged(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
                if (tab2.isSelected()) {
                	tabt1 = false;
                	try {
        				g.getChildren().remove(chart);
        				g.add(tabas, 0, 2);
        			} catch (NullPointerException n1) {
        				// TODO Auto-generated catch block
        				
        			} catch(IllegalArgumentException i2){
        				
        			}
                }
            }
        });
        
		g.setMinSize(800, 600);
		Button load = new Button();
		load.setText("Load CSV");
		Button rest = new Button();
		rest.setText("Reset Date");
		
		g.add(rest, 0, 7);
		g.add(load, 0, 1);
		DatePicker from = new DatePicker();
		DatePicker to = new DatePicker();
		g.add(from, 0, 4);
		g.add(to, 0, 5);
		ChoiceBox<String> choiceBox = new ChoiceBox();
		// g.add(choiceBox, 0, 2);
		choiceBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
                
			if (newValue.equals("Global")) {
				ShareAnalyser sh = new ShareAnalyser();
				Share global = sh.getCollectionSummary(shareCollection);
				g.getChildren().remove(chart);
				g.add(createContent(global), 0, 2);
				now = global;

			} else {
				Share temp = shareCollection.get(newValue);
				g.getChildren().remove(chart);
				g.add(createContent(temp), 0, 2);
				now = temp;
			}

		});
		rest.setOnAction(e ->{
			g.getChildren().remove(chart);
			g.add(createContent(now), 0, 2);
		});
		from.valueProperty().addListener((ov, oldValue, newValue) -> {
			ShareAnalyser s10 = new ShareAnalyser();
			Optional<Share> f;
			try {
				f = s10.getInRange(now, newValue, dateTo);
				dateFrom = newValue;
				if (f.isPresent()) {

					Share f1 = f.get();
					
					g.getChildren().remove(chart);
					g.add(createContent(f1), 0, 2);
				}
			} catch (Exception e1) {
				AlertBox a1 = new AlertBox();
				a1.display("Error", "Beginning Date has to be before ending Date");
			}

		});
		to.valueProperty().addListener((ov, oldValue, newValue) -> {
			ShareAnalyser s11 = new ShareAnalyser();

			dateTo = newValue;
			Optional<Share> f;
			try {
				f = s11.getInRange(now, dateFrom, newValue);
				if (f.isPresent()) {
					Share f1 = f.get();
					g.getChildren().remove(chart);
					g.add(createContent(f1), 0, 2);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				AlertBox a1 = new AlertBox();
				a1.display("Error", "Beginning Date has to be before ending Date");

			}

		});

		load.setOnAction(e -> {

			Stage chooser = new Stage();
			FileChooser f = new FileChooser();
			File csv = f.showOpenDialog(chooser);
			try {
				for (Share sha : shareCollection) {
					shareCollection.remove(sha.getName());
				}
				InputStream is = new FileInputStream(csv);

				CSVShareImporter imp = new CSVShareImporter();
				shareCollection = imp.load(is);
				if(shareCollection.size()==0){
					AlertBox a2 = new AlertBox();
					a2.display("Error", "Loaded Collection is Empty");
				}
				ArrayList<Share> col = (ArrayList<Share>) shareCollection.getAll();

				// System.out.println(shareCollection.toString());
				ShareAnalyser s = new ShareAnalyser();
				Share global = s.getCollectionSummary(shareCollection);
				g.getChildren().remove(chart);
				now = global;
				g.add(createContent(global), 1, 2);
				
				g.getChildren().remove(choiceBox);
				for (int i = 0; i < choiceBox.getItems().size(); i++) {
					choiceBox.getItems().remove(i);
				}

				
				for (Share k : col) {
					choiceBox.getItems().add(k.getName());
				}
				
				g.add(choiceBox, 0, 3);

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			} catch(NullPointerException n1){
				
			} catch( IllegalArgumentException i1){
				AlertBox a3 = new AlertBox();
				a3.display("Error", "Invalid CSV");
			}

		});

		Scene scene = new Scene(g);
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(100);
		g.getColumnConstraints().addAll(column1);
        
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	/**
	 * Java main for when running without JavaFX launcher
	 * 
	 * @param args
	 *            command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}
	

}
