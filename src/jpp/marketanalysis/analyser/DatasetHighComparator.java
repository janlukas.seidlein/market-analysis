package jpp.marketanalysis.analyser;

import java.util.Comparator;

import jpp.marketanalysis.data.Dataset;

public class DatasetHighComparator implements Comparator<Dataset>{

	@Override
	public int compare(Dataset o1, Dataset o2) {
		Double d1 = new Double(o1.getHigh());
		Double d2 = new Double (o2.getHigh());
		
		if( d1.compareTo(d2)!= 0){
			return d1.compareTo(d2);
		}
		else return o1.getDate().compareTo(o2.getDate());
	}
	

}
