package jpp.marketanalysis.analyser;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

import jpp.marketanalysis.data.Dataset;
import jpp.marketanalysis.data.Share;
import jpp.marketanalysis.data.ShareCollection;

public class Main {

	public static void main() {
		// TODO Auto-generated method stub+
		LocalDate zehn = LocalDate.of(2013, 7, 10);
		LocalDate elf = LocalDate.of(2013, 7, 11);
		LocalDate zwolf = LocalDate.of(2013, 7, 12);
		LocalDate dreizehn = LocalDate.of(2013, 7, 13);
		LocalDate vierzehn = LocalDate.of(2013, 7, 14);
		LocalDate funfzehn = LocalDate.of(2013, 7, 15);
		LocalDate sechzehn = LocalDate.of(2013, 7, 16);
		//Adidas
		Double eins = 1.00;
		Dataset a = new Dataset(zehn,eins,1d,1d,1d);
		Dataset b = new Dataset(elf,2d,2d,0d,2d);
		Dataset c = new Dataset(zwolf,3d,4d,3d,3d);
		Dataset d = new Dataset(dreizehn,4,5,4,4);
		ArrayList<Dataset> adida = new ArrayList<Dataset> ();
		adida.add(a);
		adida.add(b);
		adida.add(c);
		adida.add(d);
		Share adidas = new Share("Adidas",adida);
	    //Telekom
		Dataset e = new Dataset(zwolf,5,5,5,5);
		Dataset f = new Dataset(dreizehn,6,9,6,6);
		Dataset h = new Dataset(vierzehn,7,7,3,7);
		Dataset k = new Dataset(funfzehn,8,9,8,8);
		ArrayList<Dataset> tele = new ArrayList<Dataset> ();
		tele.add(e);
		tele.add(f);
		tele.add(h);
		tele.add(k);
		Share telekom = new Share("Deutsche Telekom",tele);
		//Post
		Dataset j = new Dataset(zehn,9,9,9,9);
		Dataset x = new Dataset(elf,10,10,10,10);
		Dataset y = new Dataset(funfzehn,11,11,11,11);
		Dataset z = new Dataset(sechzehn,12,12,12,12);
		ArrayList<Dataset> p = new ArrayList<Dataset> ();
		p.add(j);
		p.add(x);
		p.add(y);
		p.add(z);
		Share post = new Share("Deutsche Post",p);
		ArrayList<Share> col = new ArrayList<Share> ();
		col.add(adidas);
		col.add(telekom);
		col.add(post);
		ShareCollection co = new ShareCollection(col);
		
		Share global = ShareAnalyser.getCollectionSummary(co);
		ArrayList <Share> sa = (ArrayList <Share>) co.getAll();
		
		for(Share s : sa){
			System.out.println(s.toString());
		}
		System.out.println(global.toString());
		

	}

}