package jpp.marketanalysis.analyser;

import java.time.DayOfWeek;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import jpp.marketanalysis.data.Dataset;
import jpp.marketanalysis.data.Share;
import jpp.marketanalysis.data.ShareCollection;

public class ShareAnalyser {
	public static Dataset getLocalMinimum(Share share) {
		LocalDate l = LocalDate.now();
		Dataset result = new Dataset(l, 0, 0, Double.MAX_VALUE, 0);
		for (Dataset d : share.getData()) {
			if (d.getLow() < result.getLow()) {
				result = d;
			}
		}

		return result; // TODO
	}

	public static Dataset getLocalMaximum(Share share) {
		LocalDate l = LocalDate.now();
		Dataset result = new Dataset(l, 0, Double.MIN_VALUE, 0, 0);
		for (Dataset d : share.getData()) {
			if (d.getHigh() > result.getHigh()) {
				result = d;
			}
		}
		return result; // TODO
	}

	public static Optional<Share> getInRange(Share share, LocalDate from, LocalDate to) {
		if (from.isAfter(to)) {
			throw new IllegalArgumentException();
		}
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy"); // MMM
																				// d
																				// yyyy
																				// hh:mm
																				// a");
		String outTo = to.format(format);
		String outFrom = from.format(format);
		String name = share.getName() + " " + outFrom + "-" + outTo;
		ArrayList<Dataset> resultList = new ArrayList<Dataset>();
		for (Dataset d : share.getData()) {
			if ((d.getDate().isAfter(from) || d.getDate().isEqual(from))
					&& (d.getDate().isBefore(to) || d.getDate().isEqual(to))) {
				resultList.add(d);
			}
		}

		if (resultList.size() != 0) {
			Share resultShare = new Share(name, resultList);
			Optional<Share> result = Optional.of(resultShare);
			return result;
		} else {
			Optional<Share> result = Optional.empty();
			return result;
		}

		// return result; //TODO
	}

	public static Share getMonthly(Share share) {
		ArrayList<Dataset> result = new ArrayList<Dataset>();
		LocalDate from = share.getData().get(0).getDate();
		LocalDate to = share.getData().get(share.getData().size() - 1).getDate();
		// double[][] a = new double[12][to.getYear()-from.getYear()+1];

		for (int i = from.getYear() - 1; i < to.getYear() + 1; i++) {
			for (int k = 1; k < 13; k++) {

				LocalDate first = LocalDate.now();
				LocalDate open = LocalDate.MAX;
				LocalDate close = LocalDate.MIN;
				double openValue = 0;
				double closeValue = 0;
				double low = Double.MAX_VALUE;
				double high = Double.MIN_VALUE;

				for (Dataset d : share.getData()) {
					if (d.getDate().getMonthValue() == k && d.getDate().getYear() == i) {
						first = LocalDate.of(i, k, 1);

						if (d.getDate().isBefore(open)) {
							openValue = d.getOpen();
							open = d.getDate();
						}
						if (d.getHigh() > high) {
							high = d.getHigh();
						}
						if (d.getLow() < low) {
							low = d.getLow();
						}
						if (d.getDate().isAfter(close)) {
							closeValue = d.getClose();
							close = d.getDate();
						}
					}
				}
				if (high != Double.MIN_VALUE) {
					Dataset da = new Dataset(first, openValue, high, low, closeValue);
					result.add(da);
					// System.out.println("Adding Dataset");
				}
			}

		}
		if (result.size() != 0) {
			Share resultShare = new Share(share.getName() + " monthly", result);
			return resultShare;
		}
		return null;
	}

	public static Share getWeekly(Share share) {
		
		TemporalField woy = WeekFields.of(Locale.GERMANY).weekOfYear();
		ArrayList<Dataset> result = new ArrayList<Dataset>();
		LocalDate from = share.getData().get(0).getDate();
		LocalDate to = share.getData().get(share.getData().size() - 1).getDate();
		// double[][] a = new double[12][to.getYear()-from.getYear()+1];

		for (int i = from.getYear() - 1; i < to.getYear() + 1; i++) {
			for (int k = 1; k < 13; k++) {
				for (int j = 1; j < 54; j++) {
					LocalDate first = LocalDate.now();
					LocalDate open = LocalDate.MAX;
					LocalDate close = LocalDate.MIN;
					double openValue = 0;
					double closeValue = 0;
					double low = Double.MAX_VALUE;
					double high = Double.MIN_VALUE;

					for (Dataset d : share.getData()) {
						if (d.getDate().getYear() == i && d.getDate().get(woy) == j
								&& d.getDate().getMonthValue() == k) {

							int day = (d.getDate().getDayOfMonth() - (d.getDate().getDayOfWeek().getValue())) + 1;

							if (day > 0) {
								first = LocalDate.of(i, k, day);
							} else if (k - 1 > 0) {
								LocalDate test = LocalDate.of(i, k - 1, 5);
								int tag = test.lengthOfMonth();
								int b = tag-day;
								if(b > 28)
									b = 28;
								first = LocalDate.of(i, k - 1, b);
							} else {
								LocalDate test = LocalDate.of(i, 12, 5);
								int tag = test.lengthOfMonth();
								int b = tag-day;
								if(b > 28)
									b = 28;
								first = LocalDate.of(i - 1, 12, b);
							}
							if (d.getDate().isBefore(open)) {
								openValue = d.getOpen();
								open = d.getDate();
							}
							if (d.getHigh() > high) {
								high = d.getHigh();
							}
							if (d.getLow() < low) {
								low = d.getLow();
							}
							if (d.getDate().isAfter(close)) {
								closeValue = d.getClose();
								close = d.getDate();
							}
						}
					}
					if (high != Double.MIN_VALUE) {
						Dataset da = new Dataset(first, openValue, high, low, closeValue);
						result.add(da);

					}
				}
			}
		}
        for(Dataset sj: result){
		if (result.size() != 0) {

			LocalDate first = LocalDate.now();
			LocalDate open = LocalDate.MAX;
			LocalDate close = LocalDate.MIN;
			double openValue = 0;
			double closeValue = 0;
			double low = Double.MAX_VALUE;
			double high = Double.MIN_VALUE;
			// System.out.println(result.toString());
			// Bei IllegalArgumentExeption weitere for-Loop fuer alle
			// Datensaetze
			// einfuegen;
			for (int d = 0; d < (result.size()); d++) {
				for (int k = 0; k < (result.size()); k++) {
					// System.out.println("k: "+k+"d: "+d+"size: "+
					// result.size());
					if (result.get(d).getDate().equals(result.get(k).getDate())
							&& (result.get(d).equals(result.get(k)) == false)) {
						first = result.get(k).getDate();
						openValue = result.get(d).getOpen();
						high = result.get(d).getHigh();
						low = result.get(d).getLow();
						if (result.get(k).getHigh() > result.get(d).getHigh()) {
							high = result.get(k).getHigh();
						}
						// System.out.println("Low 1: "+low);
						if (result.get(k).getLow() < result.get(d).getLow()) {
							low = result.get(k).getLow();
						}
						// System.out.println("Low 2: "+low);
						closeValue = result.get(k).getClose();
						Dataset neu = new Dataset(first, openValue, high, low, closeValue);
						result.set(d, neu);
						result.remove(k);

					}

				}}
			}
			// System.out.println(result.toString());
			Share resultShare = new Share(share.getName() + " weekly", result);
			return resultShare;
		}
		return null;

	}

	public static Share getCollectionSummary(ShareCollection shares) {

		// for (Share s : shares.getAll()) { System.out.println(s.toString()); }

		ArrayList<Share> allShares = (ArrayList<Share>) shares.getAll();
		Collections.sort(allShares);
		ArrayList<LocalDate> dateList = getAllDates(shares);
		ArrayList<Dataset> dataSetList = new ArrayList<Dataset>();

		for (LocalDate dates : dateList) { // Erstellen der neuen Datasets
			Dataset newDataset = new Dataset(dates, 0, 0, 0, 0);

			for (Share s : allShares) {

				if (containsDate(s, dates)) {
					for (Dataset d : s.getData()) {
						if (d.getDate().equals(dates)) {

							newDataset = normalneuesDataset(d, newDataset);

						}
					}
				} else {// System.out.println(s.getName()+" "+dates.toString());
					boolean exists = false;
					ArrayList<Dataset> data = (ArrayList<Dataset>) s.getData();
					Dataset ab = null;
					int i = 0;

					/*
					 * for(int k = 0;k<data.size();k++){
					 * if(data.get(k).getDate().isAfter(d)){ if(k==0){ return
					 * Double.MAX_VALUE; } else{ return
					 * data.get(k-1).getClose(); } }
					 * 
					 * } return Double.MAX_VALUE;
					 */
					for (int h = 0; h < data.size() && data.get(h).getDate().isBefore(dates); h++) {
						ab = data.get(h);
						exists = true;
					}

					/*
					 * while(i<data.size()-1 &&
					 * data.get(i).getDate().isBefore(d)){ i++; ab =
					 * data.get(i);
					 * 
					 * }
					 */

					if (exists) {
						double e = ab.getClose();
						Dataset ahoi = new Dataset(dates, newDataset.getOpen() + e, newDataset.getHigh() + e,
								newDataset.getLow() + e, newDataset.getClose() + e);
						newDataset = ahoi;
					} else {

						Dataset h = s.getData().get(0);
						double open = h.getOpen();
						Dataset ahoi = new Dataset(dates, newDataset.getOpen() + open, newDataset.getHigh() + open,
								newDataset.getLow() + open, newDataset.getClose() + open);
						newDataset = ahoi;
					}
					/*
					 * if (existingEarlierData(dates, s) != Double.MAX_VALUE) {
					 * // Frueherer // Datensatz // existiert double e =
					 * existingEarlierData(dates, s); Dataset ahoi = new
					 * Dataset(dates, newDataset.getOpen() + e,
					 * newDataset.getHigh() + e, newDataset.getLow() + e,
					 * newDataset.getClose() + e); newDataset = ahoi; //
					 * System.out.println("eData "+s.getName()+" "+e+" //
					 * "+newDataset.getDate()); } else { // Nehme ersten
					 * Datensatz Dataset h = s.getData().get(0); double open =
					 * h.getOpen(); Dataset ahoi = new Dataset(dates,
					 * newDataset.getOpen() + open, newDataset.getHigh() + open,
					 * newDataset.getLow() + open, newDataset.getClose() +
					 * open); newDataset = ahoi; //
					 * System.out.println("!eData "+s.getName()+" "+open+" //
					 * "+newDataset.getDate()); }
					 * 
					 */ }

			}
			dataSetList.add(newDataset);
		}

		Share global = new Share("global", dataSetList);
		// System.out.println(dataSetList.toString());
		return global; // TODO
	}

	public static Dataset normalneuesDataset(Dataset d, Dataset newDataset) {

		double nOpen = d.getOpen() + newDataset.getOpen();
		double nClose = d.getClose() + newDataset.getClose();
		double nHigh = d.getHigh() + newDataset.getHigh();
		double nLow = d.getLow() + newDataset.getLow();
		Dataset aha = new Dataset(newDataset.getDate(), nOpen, nHigh, nLow, nClose);
		newDataset = aha;
		// System.out.println("nData "+" "+d.getOpen()+"
		// "+newDataset.getDate());
		return newDataset;

	}

	public static boolean containsDate(Share share, LocalDate date) {
		boolean contains = false;
		for (Dataset d : share.getData()) {
			// System.out.println(share.getName()+": "+ date+ " "+contains);
			if (d.getDate().isEqual(date)) {
				contains = true;
				// System.out.println(share.getName()+": "+ date);
				return contains;
			}
		}
		return contains;
	}

	public static ArrayList<LocalDate> getAllDates(ShareCollection shares) {
		ArrayList<LocalDate> dateList = new ArrayList<LocalDate>();
		ArrayList<Share> allShare = (ArrayList<Share>) shares.getAll();
		for (Share s : allShare) {
			for (Dataset d : s.getData()) {
				if (!dateList.contains(d.getDate())) {
					dateList.add(d.getDate());
				}
			}
		}
		return dateList;
	}

	public static double existingEarlierData(LocalDate d, Share share) {
		boolean exists = false;
		ArrayList<Dataset> data = (ArrayList<Dataset>) share.getData();
		Dataset ab = null;

		for (int h = 0; h < data.size() && data.get(h).getDate().isBefore(d); h++) {
			ab = data.get(h);
			exists = true;
		}

		if (exists) {
			return ab.getClose();
		} else {

			return Double.MAX_VALUE;
		}

	}

}
